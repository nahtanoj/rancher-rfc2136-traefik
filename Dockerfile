FROM golang:1.8

ARG GOOS='linux'
ARG GOARCH='amd64'
ARG CGO_ENABLED=0

WORKDIR /go/src/gitlab.com/recalbox/ops/rancher-rfc2136-traefik/

COPY . /go/src/gitlab.com/recalbox/ops/rancher-rfc2136-traefik/

RUN go get -v

RUN go install -a -ldflags '-s'

ENTRYPOINT ["/go/bin/external-dns"]
